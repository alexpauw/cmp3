

#Eindopdracht Drupal portfolio - Alexander Pauwelyn
--------------------------------
Docent: Dhr. Raes
OLOD: Crossmediapublishing 3
Academiejaar: 2015-2016


##Deployment guide

----------------------------------

1. blablabla
2. blablabla
3. blablabla


##Moodboard

----------------------------------

![Moodboard](images/moodboard.jpg "moodboard")

----------------------------------

##Styletile

----------------------------------

![Styletile](images/styletile.jpg "Wireframes")

##Sitemap

----------------------------------
![Sitemap](images/sitemap.jpg "Ideëenbord")


##Designs

----------------------------------
![Design1](images/design1.jpg "Design")
![Design2](images/design2.jpg "Design")
![Design3](images/design3.jpg "Design")
![Design4](images/design4.jpg "Design")
![Design5](images/design5.jpg "Design")
![Design6](images/design6.jpg "Design")




